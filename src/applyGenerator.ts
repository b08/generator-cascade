import { KeyMap } from "@b08/flat-key";
import { isFunction } from "@b08/type-checks";
import { ContentFile, GeneratorFunc, GeneratorSetup } from "./types";

export function applyGenerator<T extends ContentFile>(files: T[], generated: T[], generator: GeneratorSetup<T>): T[] {
  let inputFiles = [...files, ...generated];
  let func: GeneratorFunc<T>;
  if (isFunction(generator)) {
    func = generator as GeneratorFunc<T>;
  } else {
    const filter: (f: T) => boolean = (generator as any).filter;
    func = (generator as any).generate;
    if (filter != null) { inputFiles = inputFiles.filter(filter); }
  }
  const resultFiles = func(inputFiles);
  const map = new KeyMap((file: ContentFile) => ({ folder: file.folder, name: file.name, ext: file.extension }), resultFiles);
  const filtered = generated.filter(g => {
    return !map.has(map.keySelector(g));
  });
  const result = [...filtered, ...resultFiles];
  return result;
}
