import { except } from "@b08/array";
import { collapseToIndex } from "@b08/index-collapser";
import { applyGenerator } from "./applyGenerator";
import { CollapserOptions, ContentFile, GeneratorFunc, GeneratorSetup } from "./types";

export function cascadeGenerators<T extends ContentFile = ContentFile>(generators: GeneratorSetup<T>[],
  options?: CollapserOptions): GeneratorFunc<T> {
  return function (files: T[]): T[] {
    const generatedFiles = generators.reduce((gFiles: T[], gen: GeneratorSetup<T>) => applyGenerator(files, gFiles, gen), []);
    return collapse<T>(generatedFiles, options);
  };
}

function collapse<T extends ContentFile>(files: T[], options: CollapserOptions = {}): T[] {
  const filter = options.filter || (() => true);
  const toCollapse = files.filter(filter);
  const rest = except(files, toCollapse);
  const collapsed = collapseToIndex(toCollapse, options.options);
  return [...collapsed, ...rest];
}
