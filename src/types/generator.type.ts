import { ContentFile } from "./contentFile.type";
import { InputFilter } from "./inputFilter.type";

export type GeneratorFunc<T extends ContentFile> = (files: T[]) => T[];

export type GeneratorSetup<T extends ContentFile = ContentFile> = {
  generate: GeneratorFunc<T>;
  filter?: InputFilter;
} | GeneratorFunc<T>;
