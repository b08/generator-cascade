import { ContentFile } from "./contentFile.type";

export type InputFilter = (file: ContentFile) => boolean;
