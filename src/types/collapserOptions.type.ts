import { GeneratorOptions } from "@b08/index-collapser";
import { InputFilter } from "./inputFilter.type";

export interface CollapserOptions {
  options?: GeneratorOptions;
  filter?: InputFilter;
}
