import { ContentFile } from "@b08/index-collapser";
import { test } from "@b08/test-runner";
import { cascadeGenerators } from "../src";
import { GeneratorSetup } from "../src/types";

test("cascade returns single generated file", expect => {
  // arrange
  const file: ContentFile = {
    folder: "f",
    name: "n",
    contents: "c",
    extension: ".ts"
  };

  const setup: GeneratorSetup = {
    generate: () => [file],
  };

  // act
  const target = cascadeGenerators([setup]);
  const result = target([]);

  // assert
  const expected: ContentFile = {
    ...file,
    name: "index"
  };
  expect.deepEqual(result, [expected]);
});

test("cascade returns single generated file without collapsing", expect => {
  // arrange
  const file: ContentFile = {
    folder: "f",
    name: "n",
    contents: "c",
    extension: ".ts"
  };

  const setup: GeneratorSetup = {
    generate: () => [file],
  };

  // act
  const target = cascadeGenerators([setup], { filter: () => false });
  const result = target([]);

  // assert
  expect.deepEqual(result, [file]);
});
