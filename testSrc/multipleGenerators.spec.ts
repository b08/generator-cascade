import { ContentFile } from "@b08/index-collapser";
import { test } from "@b08/test-runner";
import { cascadeGenerators } from "../src";
import { GeneratorSetup } from "../src/types";

test("cascade returns two generated files collapsed to one", expect => {
  // arrange
  const file1: ContentFile = {
    folder: "f",
    name: "n1",
    contents: "c1\n",
    extension: ".ts"
  };

  const setup1 = () => [file1];

  const file2: ContentFile = {
    folder: "f",
    name: "n2",
    contents: "c2\n",
    extension: ".ts"
  };

  const setup2 =  () => [file2];

  // act
  const target = cascadeGenerators([setup1, setup2]);
  const result = target([]);

  // assert
  expect.equal(result.length, 1);
  const rFile = result[0];
  expect.equal(rFile.name, "index");
  expect.equal(rFile.extension, ".ts");
  expect.equal(rFile.folder, "f");
  expect.true(rFile.contents.includes("c1\n"));
  expect.true(rFile.contents.includes("c2\n"));
});

test("cascade returns one file, second file overrides first file", expect => {
  // arrange
  const file1: ContentFile = {
    folder: "f",
    name: "n1",
    contents: "c1\n",
    extension: ".ts"
  };

  const setup1: GeneratorSetup = {
    generate: () => [file1],
  };

  const file2: ContentFile = {
    folder: "f",
    name: "n1",
    contents: "c2\n",
    extension: ".ts"
  };

  const setup2: GeneratorSetup = {
    generate: () => [file2],
  };

  // act
  const target = cascadeGenerators([setup1, setup2], { filter: () => false });
  const result = target([]);

  // assert
  expect.deepEqual(result, [file2]);
});


test("cascade gives one file to first, another to second", expect => {
  // arrange
  const src = [{
    folder: "f",
    name: "n1",
    contents: "c1",
    extension: ".ts"
  },
  {
    folder: "f",
    name: "n2",
    contents: "c2",
    extension: ".ts"
  }];

  const setup1: GeneratorSetup = {
    filter: f => f.name === "n1",
    generate: (f) => [{
      ...f[0],
      contents: `${f[0].contents} ${f.length} setup1`
    }]
  };


  const setup2: GeneratorSetup = {
    filter: f => f.name === "n2",
    generate: (f) => [{
      ...f[0],
      contents: `${f[0].contents} ${f.length} setup2`
    }]
  };

  // act
  const target = cascadeGenerators([setup1, setup2], { filter: () => false });
  const result = target(src);

  // assert
  const expected = [{
    folder: "f",
    name: "n1",
    contents: "c1 1 setup1",
    extension: ".ts"
  },
  {
    folder: "f",
    name: "n2",
    contents: "c2 1 setup2",
    extension: ".ts"
  }];

  expect.deepEqual(result, expected);
  // expected.forEach(e => {
  //   const r = result.find(r => r.name === e.name);
  //   expect.deepEqual(r, e);
  // });
});
